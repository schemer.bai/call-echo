"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var CallEcho;
(function (CallEcho) {
    class CallEchoError extends Error {
        constructor(str) { super(str); }
    }
    CallEcho.CallEchoError = CallEchoError;
    class CallEchoHandler {
        constructor(v) {
            this._key = '';
            this._env = undefined;
            this._value = {};
            this._isPass = true;
            if (v)
                this._value = v;
        }
        get env() {
            if (!this._env)
                throw new CallEchoError('Env in this CEChain is undefined');
            return this._env;
        }
        get value() { return this._value; }
        get key() { return this._key; }
        get isPass() { return this._isPass; }
        stop() { this._isPass = false; }
        reset() { this._isPass = true; }
    }
    /**
     * Call an function after a stack of event,
     * and echo from the last event to first.
     *
     * @export
     * @class CallEchoChain
     * @template TValue
     */
    class CallEchoChain {
        constructor(value) {
            this._isFreezed = false;
            this.callStack = [];
            this.cbs = {};
            this._onStepCb = () => { };
            this.stepAddingTag = '';
            this.ceHandler = new CallEchoHandler(value);
        }
        freeze() {
            this._isFreezed = true;
            return this;
        }
        /** re-use this CEChain */
        with() {
            let that = new CallEchoChain();
            that.callStack = [...this.callStack];
            that.cbs = Object.assign({}, this.cbs);
            return that;
        }
        on(tag, cb) {
            if (this._isFreezed)
                throw new CallEchoError('this CEChain is freezed');
            this.stepAddingTag = tag;
            this.callStack.push(tag);
            if (!cb)
                return this;
            else
                return this.post(cb);
        }
        post(cb) {
            this._checkTagAndCollect("post" /* post */, this.stepAddingTag, cb);
            return this;
        }
        prev(cb) {
            this._checkTagAndCollect("prev" /* prev */, this.stepAddingTag, cb);
            return this;
        }
        pass(cb) {
            this._checkTagAndCollect("pass" /* pass */, this.stepAddingTag, cb);
            return this;
        }
        fail(cb) {
            this._checkTagAndCollect("fail" /* fail */, this.stepAddingTag, cb);
            return this;
        }
        /** step, callback will be called every step */
        step(cb) {
            if (this._isFreezed)
                throw new CallEchoError('this CEChain is freezed');
            this._onStepCb = cb;
            return this;
        }
        value(v, env) {
            this.ceHandler._value = v;
            this.ceHandler._env = env;
            return this;
        }
        excute(cb) {
            let reachIndex = -1;
            this.ceHandler.reset();
            // do prev and post
            for (let i = 0; i < this.callStack.length && this.ceHandler.isPass; i++) {
                reachIndex = i;
                let tag = this.callStack[i];
                this._checkTagAndProcess("prev" /* prev */, tag);
                if (this.ceHandler.isPass) {
                    this._checkTagAndProcess("post" /* post */, tag);
                }
            }
            cb && this.ceHandler.isPass && cb.call(this.ceHandler, this.ceHandler._value);
            let echoTag = this.ceHandler.isPass ? "pass" /* pass */ : "fail" /* fail */;
            for (let i = reachIndex; i >= 0; i--) {
                let tag = this.callStack[i];
                this._checkTagAndProcess(echoTag, tag);
            }
            return this.ceHandler._value;
        }
        _checkTagAndProcess(type, tag) {
            this.ceHandler._key = `${type}#${tag}`;
            let cb = this.cbs[this.ceHandler._key];
            cb && cb.call(this.ceHandler, this.ceHandler._value);
            this._onStepCb.call(this.ceHandler, this.ceHandler._value);
        }
        _checkTagAndCollect(type, tag, cb) {
            if (this._isFreezed)
                throw new CallEchoError('this CEChain is freezed');
            if (tag === '')
                throw new CallEchoError('define a tag first, on("tag").prev...');
            let key = `${type}#${tag}`;
            if (this.cbs[key])
                throw new CallEchoError('reset key of: ' + key);
            this.cbs[key] = cb;
        }
    }
    CallEcho.CallEchoChain = CallEchoChain;
    function create(value) {
        return new CallEchoChain(value);
    }
    CallEcho.create = create;
})(CallEcho || (CallEcho = {}));
exports.default = CallEcho;
