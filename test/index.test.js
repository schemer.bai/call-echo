"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const index_1 = require("../index");
let callecho = index_1.default.create;
describe('test call-echo', () => {
    it('basic usage', () => {
        let t = callecho()['ceHandler'];
        chai_1.expect(t['isPass']).is.eq(true);
        t.stop();
        chai_1.expect(t['isPass']).is.eq(false);
        callecho()
            .freeze()
            .excute();
        callecho()
            .on('action', () => { })
            .on('magic', () => { })
            .on('fire-magic', () => { })
            .excute();
        // define attack
        let attack = callecho()
            .on('action')
            .on('attack');
        // then
        attack.excute(() => { });
        let attack0 = attack.with();
        chai_1.expect(attack['callStack']).to.not.eq(attack0['callStack']);
        chai_1.expect(attack['callStack']).to.eql(attack0['callStack']);
        chai_1.expect(attack['cbs']).to.not.eq(attack0['cbs']);
        chai_1.expect(attack['cbs']).to.eql(attack0['cbs']);
    });
    it('with freezed attributes', () => {
        let attack = callecho().on('action').on('attack');
        chai_1.expect(attack['callStack']).to.eql(['action', 'attack']);
        let attack1 = attack.freeze().with();
        attack1.on('new');
        chai_1.expect(attack1['callStack']).to.eql(['action', 'attack', 'new']);
    });
    it('change types when re-use', () => {
        let action = callecho({}).on('action');
        let attack = action.with();
        let iterm = action.with();
    });
    it('test values', () => {
        callecho({ pass: true })
            .on('action')
            .excute(function () {
            chai_1.expect(this['isPass']).is.eq(true);
        });
        let ret = callecho({ pass: true })
            .on('action').post(function (ret) {
            ret.pass = false;
            this.stop();
        })
            .excute(v => { throw 'this will not excute'; });
        chai_1.expect(ret.pass).is.false;
    });
    it('test stack: basic call-cecho', () => {
        let stack = [];
        callecho()
            .on('a').on('b').on('c')
            .step(function () { stack.push(this.key); })
            .excute();
        chai_1.expect(stack).is.eql([
            'prev#a', 'post#a', 'prev#b', 'post#b', 'prev#c', 'post#c',
            'pass#c', 'pass#b', 'pass#a'
        ]);
    });
    it('test stack: basic call-cecho and cbs', () => {
        let stack0 = [];
        let stack1 = [];
        callecho()
            .on('a').prev(() => stack1.push('a')).pass(() => stack1.push('a'))
            .on('b').post(() => stack1.push('b'))
            .on('c', () => stack1.push('done')).pass(() => stack1.push('c'))
            .step(function () { stack0.push(this.key); })
            .excute();
        chai_1.expect(stack0).is.eql([
            'prev#a', 'post#a', 'prev#b', 'post#b', 'prev#c', 'post#c',
            'pass#c', 'pass#b', 'pass#a'
        ]);
        chai_1.expect(stack1).is.eql(['a', 'b', 'done', 'c', 'a']);
    });
    it('test stack: cut after post', () => {
        let stack = [];
        callecho({ pass: true })
            .on('a').on('b').on('c')
            .post(function () { this.stop(); })
            .step(function () { stack.push(this.key); })
            .excute();
        chai_1.expect(stack).is.eql([
            'prev#a', 'post#a', 'prev#b', 'post#b', 'prev#c', 'post#c',
            'fail#c', 'fail#b', 'fail#a'
        ]);
    });
    it('test stack: cut when prev', () => {
        let stack = [];
        callecho({ pass: true })
            .on('a').on('b').on('c')
            .prev(function () { this.stop(); })
            .step(function () { stack.push(this.key); })
            .excute();
        chai_1.expect(stack).is.eql([
            'prev#a', 'post#a', 'prev#b', 'post#b', 'prev#c',
            'fail#c', 'fail#b', 'fail#a'
        ]);
    });
    it('test stack: cut after b', () => {
        let stack = [];
        callecho({ pass: true }).on('a')
            .on('b').post(function () { this.stop(); })
            .on('c')
            .step(function () { stack.push(this.key); })
            .excute();
        chai_1.expect(stack).is.eql([
            'prev#a', 'post#a', 'prev#b', 'post#b',
            'fail#b', 'fail#a'
        ]);
    });
    it('test stack: cut before b', () => {
        let stack = [];
        callecho({ pass: true }).on('a')
            .on('b').prev(function () { this.stop(); })
            .on('c')
            .step(function () { stack.push(this.key); })
            .excute();
        chai_1.expect(stack).is.eql([
            'prev#a', 'post#a', 'prev#b',
            'fail#b', 'fail#a'
        ]);
    });
    it('test stack: excute CallBack', () => {
        let stack = [];
        callecho()
            .on('a').on('b')
            .step(function () { stack.push(this.key); })
            .excute(() => stack.push('do'));
        chai_1.expect(stack).is.eql([
            'prev#a', 'post#a', 'prev#b', 'post#b', 'do',
            'pass#b', 'pass#a'
        ]);
    });
    it('test stack: excute CallBack skipped', () => {
        let stack = [];
        callecho({ pass: true })
            .on('a').on('b')
            .post(function () { this.stop(); })
            .step(function () { stack.push(this.key); })
            .excute(() => stack.push('skipped'));
        chai_1.expect(stack).is.eql([
            'prev#a', 'post#a', 'prev#b', 'post#b',
            'fail#b', 'fail#a'
        ]);
    });
    it('catch error', () => {
        // do not set env error
        try {
            callecho()
                .excute(function () {
                let t = this.env;
            });
            throw 'not expected here';
        }
        catch (e) {
            chai_1.expect(e).is.instanceof(index_1.default.CallEchoError);
        }
        // change a freezed cec error
        try {
            let ce = callecho().freeze();
            ce.on('asdf');
            throw 'not expected here';
        }
        catch (e) {
            chai_1.expect(e).is.instanceof(index_1.default.CallEchoError);
        }
        // do not set tag error
        try {
            let ce = callecho().prev(v => null);
            throw 'not expected here';
        }
        catch (e) {
            chai_1.expect(e).is.instanceof(index_1.default.CallEchoError);
        }
        try {
            let ce = callecho()
                .on('a')
                .prev(v => null)
                .prev(v => null);
            throw 'not expected here';
        }
        catch (e) {
            chai_1.expect(e).is.instanceof(index_1.default.CallEchoError);
        }
    });
});
