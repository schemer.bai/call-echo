
## Usage

```js
  let stack0: string[] = []
  let stack1: string[] = []
  callecho()
    .on('a').prev(() => stack1.push('a')).pass(() => stack1.push('a'))
    .on('b').post(() => stack1.push('b'))
    .on('c', () => stack1.push('done')).pass(() => stack1.push('c'))
    .step(function() {stack0.push(this.key)})
    .excute()
  expect(stack0).is.eql([
    'prev#a', 'post#a', 'prev#b', 'post#b', 'prev#c', 'post#c',
    'pass#c', 'pass#b', 'pass#a'
  ])
  expect(stack1).is.eql(['a', 'b', 'done', 'c', 'a'])
```