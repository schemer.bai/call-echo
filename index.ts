namespace CallEcho {

  export class CallEchoError extends Error {
    constructor(str: string) { super(str) }
  }

  export interface ICallBackHandler<E, V> {
    readonly key: string
    readonly value: V
    readonly env: E
    stop(): void
  }

  class CallEchoHandler<E, V> implements ICallBackHandler<E, V> {

    _key: string = ''
    _env: E | undefined = undefined
    _value = {} as V
    _isPass = true

    constructor(v: V | undefined) {
      if (v) this._value = v
    }

    get env() {
      if (!this._env) throw new CallEchoError('Env in this CEChain is undefined')
      return this._env
    }

    get value() { return this._value as V }

    get key() { return this._key }

    get isPass() { return this._isPass }

    stop() { this._isPass = false }

    reset() { this._isPass = true }
  }

  export type CallBack<E, V> = (this: ICallBackHandler<E, V>, value: V) => void

  interface ICallBackMap<E, V> {
    [index: string]: CallBack<E, V>
  }

  export const enum StepType {
    prev = 'prev',
    post = 'post',
    pass = 'pass',
    fail = 'fail',
  }

  /**
   * Call an function after a stack of event,
   * and echo from the last event to first.
   * 
   * @export
   * @class CallEchoChain
   * @template TValue 
   */
  export class CallEchoChain<TEnv, TValue> {

    private _isFreezed = false

    private callStack: string[] = []
    private cbs: ICallBackMap<TEnv, TValue> = {}
    private _onStepCb: CallBack<TEnv, TValue> = () => {}

    private stepAddingTag: string = ''

    private ceHandler: CallEchoHandler<TEnv, TValue>

    constructor(value?: TValue) {
      this.ceHandler = new CallEchoHandler<TEnv, TValue>(value)
    }

    freeze() {
      this._isFreezed = true
      return this
    }

    /** re-use this CEChain */
    with<T extends TValue>() {
      let that = new CallEchoChain<TEnv, TValue>()
      that.callStack = [...this.callStack]
      that.cbs = { ...this.cbs }
      return that
    }

    on(tag: string, cb?: CallBack<TEnv, TValue>) {
      if (this._isFreezed) throw new CallEchoError('this CEChain is freezed')
      this.stepAddingTag = tag
      this.callStack.push(tag)
      if (!cb) return this
      else return this.post(cb)
    }

    post(cb: CallBack<TEnv, TValue>) {
      this._checkTagAndCollect(StepType.post, this.stepAddingTag, cb)
      return this
    }

    prev(cb: CallBack<TEnv, TValue>) {
      this._checkTagAndCollect(StepType.prev, this.stepAddingTag, cb)
      return this
    }

    pass(cb: CallBack<TEnv, TValue>) {
      this._checkTagAndCollect(StepType.pass, this.stepAddingTag, cb)
      return this
    }

    fail(cb: CallBack<TEnv, TValue>) {
      this._checkTagAndCollect(StepType.fail, this.stepAddingTag, cb)
      return this
    }

    /** step, callback will be called every step */
    step(cb: CallBack<TEnv, TValue>) {
      if (this._isFreezed) throw new CallEchoError('this CEChain is freezed')
      this._onStepCb = cb
      return this
    }

    value(v: TValue, env?: TEnv) {
      this.ceHandler._value = v
      this.ceHandler._env = env
      return this
    }

    excute(cb?: CallBack<TEnv, TValue>) {

      let reachIndex = -1
      this.ceHandler.reset()
      // do prev and post
      for (let i = 0; i < this.callStack.length && this.ceHandler.isPass; i++) {
        reachIndex = i
        let tag = this.callStack[i]
        this._checkTagAndProcess(StepType.prev, tag)
        if (this.ceHandler.isPass) {
          this._checkTagAndProcess(StepType.post, tag)
        }
      }

      cb && this.ceHandler.isPass && cb.call(this.ceHandler, this.ceHandler._value)

      let echoTag = this.ceHandler.isPass ? StepType.pass : StepType.fail

      for (let i = reachIndex; i >= 0; i--) {
        let tag = this.callStack[i]
        this._checkTagAndProcess(echoTag, tag)
      }

      return this.ceHandler._value
    }

    private _checkTagAndProcess(type: string, tag: string) {
      this.ceHandler._key = `${type}#${tag}`
      let cb = this.cbs[this.ceHandler._key]
      cb && cb.call(this.ceHandler, this.ceHandler._value)
      this._onStepCb.call(this.ceHandler, this.ceHandler._value)
    }

    private _checkTagAndCollect(type: string, tag: string, cb: CallBack<TEnv, TValue>) {
      if (this._isFreezed) throw new CallEchoError('this CEChain is freezed')
      if (tag === '') throw new CallEchoError('define a tag first, on("tag").prev...')
      let key = `${type}#${tag}`
      if (this.cbs[key]) throw new CallEchoError('reset key of: ' + key)
      this.cbs[key] = cb
    }

  }

  export function create<TEnv, TValue>(value?: TValue) {
    return new CallEchoChain<TEnv, TValue>(value)
  }

}

export default CallEcho