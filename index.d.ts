declare namespace CallEcho {
    class CallEchoError extends Error {
        constructor(str: string);
    }
    interface ICallBackHandler<E, V> {
        readonly key: string;
        readonly value: V;
        readonly env: E;
        stop(): void;
    }
    type CallBack<E, V> = (this: ICallBackHandler<E, V>, value: V) => void;
    const enum StepType {
        prev = "prev",
        post = "post",
        pass = "pass",
        fail = "fail",
    }
    /**
     * Call an function after a stack of event,
     * and echo from the last event to first.
     *
     * @export
     * @class CallEchoChain
     * @template TValue
     */
    class CallEchoChain<TEnv, TValue> {
        private _isFreezed;
        private callStack;
        private cbs;
        private _onStepCb;
        private stepAddingTag;
        private ceHandler;
        constructor(value?: TValue);
        freeze(): this;
        /** re-use this CEChain */
        with<T extends TValue>(): CallEchoChain<TEnv, TValue>;
        on(tag: string, cb?: CallBack<TEnv, TValue>): this;
        post(cb: CallBack<TEnv, TValue>): this;
        prev(cb: CallBack<TEnv, TValue>): this;
        pass(cb: CallBack<TEnv, TValue>): this;
        fail(cb: CallBack<TEnv, TValue>): this;
        /** step, callback will be called every step */
        step(cb: CallBack<TEnv, TValue>): this;
        value(v: TValue, env?: TEnv): this;
        excute(cb?: CallBack<TEnv, TValue>): TValue;
        private _checkTagAndProcess(type, tag);
        private _checkTagAndCollect(type, tag, cb);
    }
    function create<TEnv, TValue>(value?: TValue): CallEchoChain<TEnv, TValue>;
}
export default CallEcho;
